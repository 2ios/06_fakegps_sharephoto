//
//  DetailPhotoViewController.swift
//  FakeGPS&SharePhoto
//
//  Created by User543 on 22.08.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import SDWebImage
import Photos
import GoogleMobileAds

class DetailPhotoViewController: UIViewController, GADBannerViewDelegate {
    
    var photoURL = ""
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var adBanner: GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let url = URL(string: photoURL)
        imageView.sd_setImage(with: url)
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkProduct), name: NSNotification.Name(rawValue: "CheckProduct"), object: nil)
    }
    
    func checkProduct() {
        if !StoreManager.shared.isPurchased(id: nonconsumable1) {
            adBanner.adUnitID = keyGoogleAdsBanner
            adBanner.delegate = self
            adBanner.rootViewController = self
            let request = GADRequest()
            adBanner.load(request)
            adBanner.isHidden = false
        } else {
            adBanner.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
        self.checkProduct()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func saveButton(_ sender: UIBarButtonItem) {
        print("Salvat")
        
        // Get the current authorization state.
        let status = PHPhotoLibrary.authorizationStatus()
        
        if (status == PHAuthorizationStatus.authorized) {
            // Access has been granted.
            if imageView.image != nil {
                //Save it to the camera roll
                UIImageWriteToSavedPhotosAlbum(imageView.image!, nil, nil, nil)
            }
        }
            
        else if (status == PHAuthorizationStatus.denied) {
            // Access has been denied.
            let alert = UIAlertController(title: "Access denied",
                                          message: "Saving image needs to access your photo album",
                                          preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Settings", style: .default) { (action) -> Void in
                
                if let url = URL(string:UIApplicationOpenSettingsURLString) {
                    
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else {
                        // Fallback on earlier versions
                        UIApplication.shared.openURL(url)
                    }
                }
            })
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
                
            })
            self.present(alert, animated: true, completion: nil)
        }
            
        else if (status == PHAuthorizationStatus.notDetermined) {
            
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                
                if (newStatus == PHAuthorizationStatus.authorized) {
                    if self.imageView.image != nil {
                        //Save it to the camera roll
                        UIImageWriteToSavedPhotosAlbum(self.imageView.image!, nil, nil, nil)
                    }
                }
                    
                else {
                    
                }
            })
        }
            
        else if (status == PHAuthorizationStatus.restricted) {
            // Restricted access - normally won't happen.
            let alert = UIAlertController(title: "Access restricted",
                                          message: "Saving image needs to access your photo album",
                                          preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Settings", style: .default) { (action) -> Void in
                
                if let url = URL(string:UIApplicationOpenSettingsURLString) {
                    
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else {
                        // Fallback on earlier versions
                        UIApplication.shared.openURL(url)
                    }
                }
            })
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
                
            })
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    deinit {
        print("DEINIT DETAIL SUCCES")
    }
}
