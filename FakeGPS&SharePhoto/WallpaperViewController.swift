//
//  WallpaperViewController.swift
//  FakeGPS&SharePhoto
//
//  Created by User543 on 18.08.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import SDWebImage
import GoogleMobileAds

class WallpaperViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UISearchBarDelegate, UITabBarControllerDelegate, GADBannerViewDelegate {
    
    var photoSum = [NSMutableDictionary]()
    var pages = 1
    var totalPages = 0
    var queryString = "Nature"
    var indexPhoto = 0
    var alert: UIAlertController?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var adBanner: GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tabBarController?.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.catchNotification(notification:)), name: NSNotification.Name(rawValue: "CatchNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkProduct), name: NSNotification.Name(rawValue: "CheckProduct"), object: nil)
        showWaiting {
            self.queryUnsplash()
        }
    }
    
    func checkProduct() {
        if !StoreManager.shared.isPurchased(id: nonconsumable1) {
            adBanner.adUnitID = keyGoogleAdsBanner
            adBanner.delegate = self
            adBanner.rootViewController = self
            let request = GADRequest()
            adBanner.load(request)
            adBanner.isHidden = false
        } else {
            adBanner.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkProduct()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if viewController == self {
            self.queryUnsplash()
        } else {
            SDImageCache.shared().clearMemory()
            SDImageCache.shared().clearDisk()
            self.photoSum = []
            self.pages = 1
            self.collectionView.reloadData()
        }
    }
    
    // MARK: CollectionView Data Source
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photoSum.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let lastRowIndex = collectionView.numberOfItems(inSection: 0) - 1
        if indexPath.item == lastRowIndex && self.photoSum.count != 0 {
            self.pages += 1
            if self.pages <= self.totalPages {
                self.queryUnsplash()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var n = 0
        if UI_USER_INTERFACE_IDIOM() == .pad {
            n = 6
        } else {
            n = 3
        }
        let width = self.collectionView.frame.width - CGFloat((n-1)*10)
        let widthCell = width / CGFloat(n)
        let heightCell = widthCell
        return CGSize(width: widthCell, height: heightCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! WallpersCollectionViewCell
        let photoUrl = self.photoSum[indexPath.row].object(forKey: "thumbURL") as! String
        let url = URL(string: photoUrl)
        cell.imageView.sd_setImage(with: url)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        searchBar.endEditing(true)
        indexPhoto = indexPath.row
        performSegue(withIdentifier: "ToDetailViewController", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToDetailViewController" {
            let DetailVC: DetailPhotoViewController = segue.destination as! DetailPhotoViewController
            DetailVC.photoURL = photoSum[indexPhoto].object(forKey: "regularURL") as! String
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.queryString = searchBar.text!
        
        self.photoSum = []
        self.collectionView.reloadData()
        self.pages = 1
        searchBar.endEditing(true)
        showWaiting {
            self.queryUnsplash()
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.text = ""
        searchBar.endEditing(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchBar.endEditing(true)
    }
    
    
    func queryUnsplash() {
        
        let url = URL(string: "https://unsplash.com/napi/search/photos?query=\(self.queryString)&xp=&per_page=50&page=\(self.pages)")
        var request = URLRequest(url: url!, cachePolicy: .useProtocolCachePolicy , timeoutInterval: 60.0)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Client-ID c94869b36aa272dd62dfaeefed769d4115fb3189a9d1ec88ed457207747be626", forHTTPHeaderField: "authorization")
        request.httpMethod = "GET"
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        let postDataTask = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("Error queryUnsplashfunction \(String(describing: error))")
                return
            }
            guard let data = data else {
                print("Data was not recieved queryUnsplashfunction")
                return
            }
            do {
                guard let requestReply = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
                    print("Error requestReply queryUnsplashfunction")
                    return
                }
                if requestReply.object(forKey: "results") != nil {
                    if (requestReply.object(forKey: "total_pages") as! Int) > 0 {
                        self.totalPages = requestReply.object(forKey: "total_pages") as! Int
                        let results = requestReply.object(forKey: "results") as! NSArray
                        for result in results {
                            let urls = (result as! NSDictionary).object(forKey: "urls")
                            let regularURL = (urls as! NSDictionary).object(forKey: "regular") as! String
                            let thumbURL = (urls as! NSDictionary).object(forKey: "thumb") as! String
                            let dict = NSMutableDictionary()
                            dict.setValue(regularURL, forKey: "regularURL")
                            dict.setValue(thumbURL, forKey: "thumbURL")
                            self.photoSum.append(dict)
                        }
                        DispatchQueue.main.async(execute: {
                            self.collectionView.reloadData()
                            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                                            object: nil,
                                                            userInfo:["title":"nil", "message":"nil"])
                        })
                    } else {
                            print("REQUEST FINISAT")
                            DispatchQueue.main.async(execute: {
                                self.collectionView.reloadData()
                                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                                                object: nil,
                                                                userInfo:["title":"Sorry", "message":"No match found!"])
                            })
                        }
                } else {
                    DispatchQueue.main.async(execute: {
                        self.collectionView.reloadData()
                        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                                        object: nil,
                                                        userInfo:["title":"Sorry", "message":"No match found!"])
                    })
                }
            } catch let error as NSError {
                print("Error catch queryUnsplashfunction \(error.localizedDescription)")
            }
        }
        postDataTask.resume()
    }
    
    // MARK: Show waiting spinner
    func showWaiting(completion: (() -> Void)? = nil) -> Void {
        if alert == nil {
            alert = UIAlertController(title: "", message: "Please wait\n\n\n", preferredStyle: .alert)
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            spinner.center = CGPoint(x: 135.0, y: 65.5)
            spinner.color = UIColor.black
            spinner.startAnimating()
            alert?.view.addSubview(spinner)
        }
        let topVC = self.getCurrentViewController(self)!
        topVC.present(alert!, animated: true, completion: completion)
    }
}
