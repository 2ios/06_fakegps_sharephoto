//
//  AdsViewController.swift
//  FakeGPS&SharePhoto
//
//  Created by User543 on 24.08.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class AdsViewController: UIViewController {
    
    var alert: UIAlertController?
    
    @IBOutlet weak var premiumImageView: UIImageView!
    @IBOutlet weak var tryFreeButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.addGesture()
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkSubscription), name: NSNotification.Name(rawValue: "CheckSubscription"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func actionTryFreeButton(_ sender: UIButton) {
        print("APASAT")
        self.goToBuy()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 1.0, delay: 1.0, options: [.repeat, .autoreverse, .allowUserInteraction], animations: {
            
            self.tryFreeButton.alpha = 0.3
            
        }, completion: nil)
    }

    func addGesture() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(goToBuy))
        self.premiumImageView.addGestureRecognizer(gesture)
        self.premiumImageView.isUserInteractionEnabled = true
    }
    
    func goToBuy() {
        showWaiting(completion: {
            if StoreManager.shared.productsFromStore.count > 1 {
                let product = StoreManager.shared.productsFromStore[1]
                StoreManager.shared.buy(product: product)
            } else {
                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                                object: nil,
                                                userInfo:["title":"Sorry", "message":"Unkown error while proccessing. Please try again!"])
            }
        })
    }
    
    func checkSubscription() {
        DispatchQueue.main.async {
            if UserDefaults.standard.bool(forKey: "Subscription") {
                self.dismiss(animated: false, completion: nil)
                print("SUBSCRIPTION ESTE")
            }
            if self.isKind(of: AdsViewController.self) {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    // MARK: Show waiting spinner
    func showWaiting(completion: (() -> Void)? = nil) -> Void {
        if alert == nil {
            alert = UIAlertController(title: "", message: "Please wait\n\n\n", preferredStyle: .alert)
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            spinner.center = CGPoint(x: 135.0, y: 65.5)
            spinner.color = UIColor.black
            spinner.startAnimating()
            alert?.view.addSubview(spinner)
        }
        let topVC = self.getCurrentViewController(self)!
        topVC.present(alert!, animated: true, completion: completion)
    }
}
