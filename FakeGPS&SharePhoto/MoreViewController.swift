//
//  MoreViewController.swift
//  FakeGPS&SharePhoto
//
//  Created by User543 on 18.08.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import GoogleMobileAds

class MoreViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, GADBannerViewDelegate {
    
    
    let arrayText = ["Subscription policy", "Privacy policy","Remove Ads forever!", "Restore", "Premium Account", "Clear Cache"]
    let arrayImage = [#imageLiteral(resourceName: "info_mod"),#imageLiteral(resourceName: "info_mod"),#imageLiteral(resourceName: "restore"),#imageLiteral(resourceName: "restore"),#imageLiteral(resourceName: "restore"),#imageLiteral(resourceName: "clear_cache")]
    var alert: UIAlertController?
    var url: String = ""
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var adBanner: GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.catchNotification(notification:)), name: NSNotification.Name(rawValue: "CatchNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkProduct), name: NSNotification.Name(rawValue: "CheckProduct"), object: nil)
        
        self.tableView.tableFooterView = UIView()
    }
    
    func checkProduct() {
        if !StoreManager.shared.isPurchased(id: nonconsumable1) {
            adBanner.adUnitID = keyGoogleAdsBanner
            adBanner.delegate = self
            adBanner.rootViewController = self
            let request = GADRequest()
            adBanner.load(request)
            adBanner.isHidden = false
        } else {
            adBanner.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkProduct()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayText.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoreViewController", for: indexPath)
        cell.textLabel?.text = self.arrayText[indexPath.row]
        cell.imageView?.image = self.arrayImage[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.url = "http://devios.mobi/max_fm_res/fake_gps/subscription_info_fake.htm"
            performSegue(withIdentifier: "ToWebViewController", sender: nil)
        }
        
        if indexPath.row == 1 {
            self.url = "http://devios.mobi/max_fm_res/fake_gps/privacy_fake_gps.htm"
            performSegue(withIdentifier: "ToWebViewController", sender: nil)
        }
        
        if indexPath.row == 2 {
            showWaiting(completion: {
                if StoreManager.shared.productsFromStore.count > 0 {
                    let product = StoreManager.shared.productsFromStore[0]
                    StoreManager.shared.buy(product: product)
                } else {
                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                                    object: nil,
                                                    userInfo:["title":"Sorry", "message":"Unkown error while proccessing. Please try again!"])
                }
            })
        }
        if indexPath.row == 3 {
            showWaiting(completion: {
                StoreManager.shared.restoreAllPurchases()
            })
        }
        if indexPath.row == 4 {
            showWaiting(completion: {
                if StoreManager.shared.productsFromStore.count > 1 {
                    let product = StoreManager.shared.productsFromStore[1]
                    StoreManager.shared.buy(product: product)
                } else {
                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                                    object: nil,
                                                    userInfo:["title":"Sorry", "message":"Unkown error while proccessing. Please try again!"])
                }
            })
        }
        if indexPath.row == 5 {
            let sharedCache = URLCache(memoryCapacity: 0, diskCapacity: 0, diskPath: nil)
            URLCache.shared = sharedCache
            let alert = UIAlertController(title: "Congratulations",
                message:"Process done",
                preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToWebViewController" {
            let WebVC: WebViewController = segue.destination as! WebViewController
            WebVC.url = self.url
        }
    }
    
    // MARK: Show waiting spinner
    func showWaiting(completion: (() -> Void)? = nil) -> Void {
        if alert == nil {
            alert = UIAlertController(title: "", message: "Please wait\n\n\n", preferredStyle: .alert)
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            spinner.center = CGPoint(x: 135.0, y: 65.5)
            spinner.color = UIColor.black
            spinner.startAnimating()
            alert?.view.addSubview(spinner)
        }
        let topVC = self.getCurrentViewController(self)!
        topVC.present(alert!, animated: true, completion: completion)
    }
}
