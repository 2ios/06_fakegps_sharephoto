//
//  AddPhotoViewController.swift
//  FakeGPS&SharePhoto
//
//  Created by User543 on 21.08.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AddPhotoViewController: UIViewController, GADBannerViewDelegate, FusumaDelegate {
    
    var image = UIImage()
    var count = 0
    var alert: UIAlertController?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var adBanner: GADBannerView!
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var screenView: UIView!
    @IBOutlet weak var premiumImageView: UIImageView!
    @IBOutlet weak var addButon: UIButton!
    
    @IBOutlet weak var tryFreeButton: UIButton!
    
    @IBAction func actionTryFreeButton(_ sender: UIButton) {
        print("APASAT")
        self.goToBuy()
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        imageView.image = image
        NotificationCenter.default.addObserver(self, selector: #selector(self.catchNotification(notification:)), name: NSNotification.Name(rawValue: "CatchNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkProduct), name: NSNotification.Name(rawValue: "CheckProduct"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkSubscription), name: NSNotification.Name(rawValue: "CheckSubscription"), object: nil)
        if UserDefaults.standard.bool(forKey: "isFirst") == false {
            UserDefaults.standard.set(0, forKey: "count")
            UserDefaults.standard.set(true, forKey: "isFirst")
        } else {
            self.count = UserDefaults.standard.integer(forKey: "count")
        }
        self.addGesture()
    }
    
    func addGesture() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(goToBuy))
        self.premiumImageView.addGestureRecognizer(gesture)
        self.premiumImageView.isUserInteractionEnabled = true
    }
    
    func goToBuy() {
        showWaiting(completion: {
            if StoreManager.shared.productsFromStore.count > 1 {
                let product = StoreManager.shared.productsFromStore[1]
                StoreManager.shared.buy(product: product)
            } else {
                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                                object: nil,
                                                userInfo:["title":"Sorry", "message":"Unkown error while proccessing. Please try again!"])
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkProduct() {
        if !StoreManager.shared.isPurchased(id: nonconsumable1) {
            adBanner.adUnitID = keyGoogleAdsBanner
            adBanner.delegate = self
            adBanner.rootViewController = self
            let request = GADRequest()
            adBanner.load(request)
            adBanner.isHidden = false
        } else {
            adBanner.isHidden = true
        }
    }
    
    func checkSubscription() {
        DispatchQueue.main.async {
            self.premiumImageView.isHidden = true
            self.tryFreeButton.isHidden = true
            self.addButon.isHidden = false
            print("SUBSCRIPTION ESTE")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
        avatarView.isHidden = true
        self.checkProduct()
    }
    
    @IBOutlet weak var avatarButtom: NSLayoutConstraint!
    
    override func viewWillLayoutSubviews() {
        if StoreManager.shared.isPurchased(id: nonconsumable1) {
            if UIDevice.current.userInterfaceIdiom == .pad {
                avatarButtom.constant = -90
            } else {
                avatarButtom.constant = -50
            }
        } else {
            if UIDevice.current.userInterfaceIdiom == .pad {
                avatarButtom.constant = 0
            } else {
                avatarButtom.constant = 0
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
        self.premiumImageView.isHidden = true
        self.tryFreeButton.isHidden = true
    }
    
    @IBAction func addPhotoButton(_ sender: UIButton) {
        print("APASAT")
        let fusuma = FusumaViewController()
        fusuma.delegate = self
        fusuma.availableModes = [.library, .camera]
        fusuma.cropHeightRatio = 0.6
        self.present(fusuma, animated: true, completion: nil)
    }
    
    @IBAction func shareButton(_ sender: UIBarButtonItem) {
        
        self.count += 1
        UserDefaults.standard.set(self.count, forKey: "count")
        if ((StoreManager.shared.receiptManager.isSubscribed) || self.count < 4) {
            self.premiumImageView.isHidden = true
            self.tryFreeButton.isHidden = true
            self.addButon.isHidden = false
            // image to share
            UIGraphicsBeginImageContext(screenView.frame.size)
            screenView.layer.render(in: UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // set up activity view controller
            let imageToShare = [ image! ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
//            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
//            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
//            activityViewController.popoverPresentationController?.sourceRect = CGRect(x: self.view.frame.origin.x, y: 0, width: 0, height: 0)
            // exclude some activity types from the list (optional)
            //        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
        } else {
            self.addButon.isHidden = true
            self.premiumImageView.isHidden = false
            self.tryFreeButton.isHidden = false
            UIView.animate(withDuration: 1.0, delay: 1.0, options: [.repeat, .autoreverse, .allowUserInteraction], animations: {
        
                self.tryFreeButton.alpha = 0.3
                
            }, completion: nil)
        }
    }
    
    
    // MARK: FusumaDelegate Protocol
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        
        
        switch source {
            
        case .camera:
            
            print("Image captured from Camera")
            
        case .library:
            
            print("Image selected from Camera Roll")
            
        default:
            
            print("Image selected")
        }
    }
    
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
    }
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode, metaData: ImageMetadata) {
        
        print("Image mediatype: \(metaData.mediaType)")
        print("Source image size: \(metaData.pixelWidth)x\(metaData.pixelHeight)")
        print("Creation date: \(String(describing: metaData.creationDate))")
        print("Modification date: \(String(describing: metaData.modificationDate))")
        print("Video duration: \(metaData.duration)")
        print("Is favourite: \(metaData.isFavourite)")
        print("Is hidden: \(metaData.isHidden)")
        print("Location: \(String(describing: metaData.location))")
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
    }
    
    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
        
        avatarView.image = image
        avatarView.isHidden = false
        
        switch source {
            
        case .camera:
            
            print("Called just after dismissed FusumaViewController using Camera")
            
        case .library:
            
            print("Called just after dismissed FusumaViewController using Camera Roll")
            
        default:
            
            print("Called just after dismissed FusumaViewController")
        }
    }
    
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
        
        let alert = UIAlertController(title: "Access Requested",
                                      message: "Saving image needs to access your photo album",
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Settings", style: .default) { (action) -> Void in
            
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    UIApplication.shared.openURL(url)
                }
            }
        })
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            
        })
        
        guard let vc = UIApplication.shared.delegate?.window??.rootViewController,
            let presented = vc.presentedViewController else {
                
                return
        }
        
        presented.present(alert, animated: true, completion: nil)
    }
    
    func fusumaClosed() {
        
        print("Called when the FusumaViewController disappeared")
    }
    
    func fusumaWillClosed() {
        
        print("Called when the close button is pressed")
    }
    
    // MARK: Show waiting spinner
    func showWaiting(completion: (() -> Void)? = nil) -> Void {
        if alert == nil {
            alert = UIAlertController(title: "", message: "Please wait\n\n\n", preferredStyle: .alert)
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            spinner.center = CGPoint(x: 135.0, y: 65.5)
            spinner.color = UIColor.black
            spinner.startAnimating()
            alert?.view.addSubview(spinner)
        }
        let topVC = self.getCurrentViewController(self)!
        topVC.present(alert!, animated: true, completion: completion)
    }
}
