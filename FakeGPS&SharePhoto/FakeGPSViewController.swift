//
//  ViewController.swift
//  FakeGPS&SharePhoto
//
//  Created by User543 on 18.08.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import GoogleMobileAds

protocol HandleMapSearch: class {
    func dropPinZoomIn(placemark:MKPlacemark)
}

class FakeGPSViewController: UIViewController, CLLocationManagerDelegate, GADBannerViewDelegate, UISearchControllerDelegate, UISearchBarDelegate {
    
    let locationManager = CLLocationManager()
    var resultSearchController: UISearchController?
    var selectedPin: MKPlacemark?
    var mapType = 0
    var alert: UIAlertController?
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var changeMode: UIButton!
    @IBOutlet weak var adBanner: GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        locationManager.delegate = self
        
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestLocation()
        }
        
        let searchTableViewController = storyboard!.instantiateViewController(withIdentifier: "SearchTableViewController") as! SearchTableViewController
        resultSearchController = UISearchController(searchResultsController: searchTableViewController)
        resultSearchController?.searchResultsUpdater = searchTableViewController
        resultSearchController?.delegate = self
        let searchBar = resultSearchController!.searchBar
        searchBar.placeholder = "Search for places"
        navigationItem.titleView = resultSearchController?.searchBar
        navigationItem.rightBarButtonItem?.isEnabled = false
        resultSearchController?.hidesNavigationBarDuringPresentation = false
        resultSearchController?.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        searchTableViewController.mapView = mapView
        searchTableViewController.handleMapSearchDelegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.catchNotification(notification:)), name: NSNotification.Name(rawValue: "CatchNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkProduct), name: NSNotification.Name(rawValue: "CheckProduct"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkSubscription), name: NSNotification.Name(rawValue: "CheckSubscription"), object: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkProduct() {
        if !StoreManager.shared.isPurchased(id: nonconsumable1) {
            adBanner.adUnitID = keyGoogleAdsBanner
            adBanner.delegate = self
            adBanner.rootViewController = self
            let request = GADRequest()
            adBanner.load(request)
            adBanner.isHidden = false
        } else {
            adBanner.isHidden = true
        }
    }
    
    func checkSubscription() {
            DispatchQueue.main.async {
                print("SUBSCRIPTION ESTE")
            }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.checkProduct()
        
        if StoreFinder.userRatedApp() {
            if !StoreManager.shared.receiptManager.isSubscribed {
                UserDefaults.standard.set(false, forKey: "Subscription")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier :"AdsViewController")
                self.present(viewController, animated: false)
            }
        }
    }
    
    @IBAction func nextButton(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "ToAddPhotoViewController", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        UIGraphicsBeginImageContext(mapView.frame.size)
        mapView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if segue.identifier == "ToAddPhotoViewController" {
            let AddPhotoVC: AddPhotoViewController = segue.destination as! AddPhotoViewController
            AddPhotoVC.image = image!
        }
    }
    
    
    @IBAction func changeModeSatelite(_ sender: UIButton) {
        switch mapType {
        case 0:
            mapView.mapType = .hybrid
            mapType += 1
        case 1:
            mapView.mapType = .satellite
            mapType += 1
        case 2:
            mapView.mapType = .standard
            mapType = 0
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            print("location:: \(location)")
            let annotation = MKPointAnnotation()
            annotation.coordinate = location.coordinate
            mapView.addAnnotation(annotation)
            let span = MKCoordinateSpanMake(0.05, 0.05)
            let region = MKCoordinateRegion(center: location.coordinate, span: span)
            mapView.setRegion(region, animated: true)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error.localizedDescription)")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        resultSearchController?.searchBar.showsCancelButton = false
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text != nil {
            
        }
        searchBar.endEditing(true)
        view.endEditing(true)
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Ads load successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Error \(error.localizedDescription)")
    }
}

extension FakeGPSViewController: HandleMapSearch {
    
    func dropPinZoomIn(placemark: MKPlacemark){
        navigationItem.rightBarButtonItem?.isEnabled = true
        navigationController?.navigationItem.rightBarButtonItem?.isEnabled = true
        // cache the pin
        selectedPin = placemark
        // clear existing pins
        mapView.removeAnnotations(mapView.annotations)
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        annotation.title = placemark.name
        
        if let city = placemark.locality,
            let state = placemark.administrativeArea {
            annotation.subtitle = "\(city) \(state)"
        }
        
        mapView.addAnnotation(annotation)
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegionMake(placemark.coordinate, span)
        mapView.setRegion(region, animated: true)
    }
}

extension FakeGPSViewController : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?{
        
        guard !(annotation is MKUserLocation) else { return nil }
        let reuseId = "pin"
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
        }
        pinView?.pinTintColor = UIColor.orange
        pinView?.canShowCallout = true
        let smallSquare = CGSize(width: 30, height: 30)
        let button = UIButton(frame: CGRect(origin: CGPoint.zero, size: smallSquare))
        button.setBackgroundImage(#imageLiteral(resourceName: "tab_search_press"), for: .normal)
        //        button.addTarget(self, action: #selector(ViewController.getDirections), forControlEvents: .TouchUpInside)
        pinView?.leftCalloutAccessoryView = button
        
        return pinView
    }
}

