//
//  WebViewController.swift
//  FakeGPS&SharePhoto
//
//  Created by User543 on 24.08.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class WebViewController: UIViewController, UIWebViewDelegate {

    var url: String = ""
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let url = URL(string: self.url)
        let request = URLRequest(url: url!)
        webView.loadRequest(request)
    }
    
}
